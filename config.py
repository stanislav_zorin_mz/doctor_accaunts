import os


class Config(object):
    
    DEBUG = True
    CSRF_ENABLED = True
    SECRET_KEY = 'to do change key'

    basedir = os.path.abspath(os.path.dirname(__file__))
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///'+os.path.join(basedir, 'db', 'app.db')
    #SQLALCHEMY_TRACK_MODIFICATIONS = False



class ProductionConfig(Config):
    DEBAG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBAG = True