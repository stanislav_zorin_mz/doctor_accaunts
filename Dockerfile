FROM python:3.9-slim-bullseye
WORKDIR /opt/webapp
EXPOSE 5000
COPY requirements.txt /opt/webapp/
RUN pip install -r ./requirements.txt
COPY webapp /opt/webapp/
ENTRYPOINT [ "gunicorn" ]
CMD ["--bind", "0.0.0.0:5000", "wsgi:app"]