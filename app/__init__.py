import os
from flask import Flask
from .database import db, migrate


def create_app():
    app = Flask(__name__)
    app.config.from_object('config.DevelopmentConfig')
    
    db.init_app(app)
    migrate.init_app(app, db)
    with app.app_context():
        db.create_all()
    
    import app.users.views as users
    import app.accaunts.views as accaunts
    app.register_blueprint(accaunts.module)
    app.register_blueprint(users.module, url_prefix = '/users' )

    return app    
