from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, validators


class NewEngForm(FlaskForm):
    username = StringField('Имя пользователя или Email', [validators.DataRequired(), validators.Length(max=60)]) 
    password = PasswordField('Укажите пароль', [validators.DataRequired(), validators.Length(max = 25)])
    confirm_password = PasswordField('Подтвердите пароль', [validators.DataRequired(), validators.Length(max=25)])
    is_admin = BooleanField('Пользователь является администратором')
    submit = SubmitField('Создать пользователя')   



