from app.database import db
from bcrypt import hashpw, gensalt, checkpw



class Engineer(db.Model):
    __tablename__ = 'engineers' 
    engineer_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(128))
    is_admin = db.Column(db.Boolean)
    
    def __repr__(self):
        return '<Engineer %r>' % self.name


class Center(db.Model):
    __tablename__ = 'centers'
    center_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True, unique=True)
    local_ip = db.Column(db.String, unique=True)
    lan = db.Column(db.String(16))
    global_ip = db.Column(db.String, unique=True)

    def __repr__(self):
        return '<Center %r>' % self.name


def get_eng_by_id(id):
    return Engineer.query.get(id)


def get_all_eng():
    return Engineer.query.all()    


def  get_eng_by_name(username):
    return Engineer.query.filter_by(name = username).first()   


def create_engineer(name, password, is_admin):
    if not get_eng_by_name(name):
        hash_password = hashpw(password.encode(), gensalt())
        eng = Engineer(name=name, password=hash_password, is_admin=is_admin)
        db.session.add(eng)
        db.session.commit() 


def delete_engineer(eng):
    db.session.delete(eng)
    db.session.commit()


def auth_engineer(username, password):
    eng = get_eng_by_name(username)
    if eng:
        if checkpw(password.encode(),eng.password):        
            return True                 
    return False



