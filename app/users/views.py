from crypt import methods
from functools import wraps
from flask import Blueprint, flash, render_template, redirect, url_for, session, g, request
from .forms import NewEngForm
from .models import db, create_engineer, get_eng_by_name, get_all_eng, delete_engineer
from .decorators import authenticate


module = Blueprint('users', __name__)



@module.route('/')
@authenticate
def list():
    users_list = get_all_eng()
    return render_template('users_list.html', list = users_list)


@module.route('/new', methods=['POST','GET'])
@authenticate
def new():
    form = NewEngForm()
    if form.validate_on_submit():
        if form.password.data == form.confirm_password.data:
            try:
                create_engineer(form.username.data, form.password.data, form.is_admin.data)
                return redirect(url_for('users.list'))
            except:    
                flash('Что-то пошло не так. Не удалось создать пользователя.')
        else:
            flash('Введенные пароли отличаются.')    

    return render_template('new_eng.html', form=form)  


@module.route('/delete/<user>', methods=['get'])
@authenticate
def delete_user(user):
    eng = get_eng_by_name(user)
    delete_engineer(eng)
    return redirect(url_for('users.list'))




