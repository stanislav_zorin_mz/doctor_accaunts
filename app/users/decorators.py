from functools import wraps
from flask import g, flash, redirect, url_for, abort, session
from app.users.models import get_eng_by_id

def authenticate(f):
    @wraps(f)
    def decorated_func(*args, **kwargs):
        if not 'user' in session:
            return redirect(url_for('accaunts.login'))
        else:
            user = get_eng_by_id(session.get('user'))
            if not user.is_admin  and f.__name__ in ('new','list','delete_user'):
                abort(401)
            else:
                return f(*args, **kwargs)
    return decorated_func