from paramiko import SSHClient, AutoAddPolicy
from string import ascii_letters
from random import choice
from transliterate import translit
import os


class CertGenError(Exception):
    def __init__(self, text) -> None:
        self.text = text

class CreateAccauntOnHostError(Exception):
    def __init__(self, text) -> None:
       self.text = text       


def generate_password(length:int) -> str:
    '''
    Генерирует случайный пароль из заглавных
    и строчных латинских букв длиной length 
    '''
    result = ''
    while len(result) <= length:
        symbol = choice(ascii_letters) 
        result = result+symbol
    return result


def create_accaunt_cert(accaunt:str):
    """
    Ищет в дирректории ca_dir файлы сертификата и 
    закрытого ключа для учетной записи accaunt. Если файлов нет, то
    генерирует их с помощью скрипта easyrsa
    """
    ca_dir = '/opt/webapp/rsa'
    try:
        if not accaunt+'.crt' in os.listdir(ca_dir+'/pki/issued'):
            os.system(ca_dir+'/easyrsa build-client-full '+accaunt+' nopass') 
        with open(ca_dir+'/pki/issued/'+accaunt+'.crt') as f:
            crt = f.read()
        with open(ca_dir+'/pki/private/'+accaunt+'.key') as f:
            key = f.read()
        return crt, key
    except:
        raise CertGenError("Ошибка создания сертификата пользователя")



def create_accaunt_parametrs(accaunt_rus:str, aetitle='')->tuple:
     #транслит имени врача в латиницу
    accaunt_eng = translit(accaunt_rus, reversed=True)   
     # убираем апострофы если есть
    accaunt_eng = accaunt_eng.replace("'", "")
    
    if aetitle == '':
        aetitle = accaunt_eng.upper()
    crt, key = create_accaunt_cert(accaunt_eng.lower())
    password = generate_password(8)
    
    return accaunt_eng.lower(), password, aetitle, crt, key 


def create_account_on_host(center_ip, accaunt, accaunt_ip, accaunt_pass, accaunt_profile='ovpn_doctors', recreate=True):
    user = 'mz'
    ssh = SSHClient()
    ssh.set_missing_host_key_policy(AutoAddPolicy())
    try:
        ssh.connect(hostname=center_ip, username=user, key_filename = '/home/ssl/createconf/id_rsa')
        stdin, stdout, stderr = ssh.exec_command('ppp secret print')
        for line in stdout:
            if recreate and ' '+accaunt+' ' in line:
                stdin, stdout, stderr = ssh.exec_command('ppp secret remove '+accaunt)

        stdin, stdout, stderr = ssh.exec_command('ppp secret add name='+accaunt+' password='+accaunt_pass+' service=ovpn profile='+accaunt_profile+' remote-address='+accaunt_ip)
        if stderr:
            raise CreateAccauntOnHostError(f"Не удалось зарегистрировать учетную запись врача на оборудовании в ДЦ. Ошибка: {stderr}")    
    except:
        raise CreateAccauntOnHostError(f"Не удалось зарегистрировать учетную запись врача на оборудовании в ДЦ.")
    finally:    
        ssh.close()


def get_base_config():
    return  'proto tcp-client\n'\
            'dev tun\n'\
            'auth-nocache\n'\
            'nobind\n'\
            'persist-key\n'\
            'tls-client\n'\
            'verb 3\n'\
            'cipher AES-256-CBC\n'\
            'auth SHA1\n'\
            'pull\n'\
            'route-method exe\n'\
            'auth-user-pass usr.cfg\n'


def create_config(center_name, center_ip, center_lan, accaunt_family, crt, key):
    with open('/opt/webapp/rsa/pki/ca.crt') as f:
        ca = f.read()  
    return f";Врач: {accaunt_family} ДЦ: {center_name}\n remote {center_ip} 1197\n {get_base_config()}\n route {center_lan} 255.255.255.0 172.16.134.1\n" \
    "<ca>\n{ca}</ca>\n<cert>{crt}</cert>\n<key>{key}</key> "
         
    

