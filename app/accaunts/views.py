from flask import Blueprint, flash, render_template, send_file, abort, redirect, url_for, session
from .models import Accaunt, write_accaunt_to_db, get_auth_data
from .utils import create_account_on_host, create_config, CreateAccauntOnHostError, CertGenError 
from .forms import NewAccauntForm, GetConfForm, LoginForm
from app.users.models import Center, get_eng_by_name, auth_engineer
from app.users.decorators import authenticate
from io import BytesIO 


module = Blueprint('accaunts', __name__)       


@module.route('/')
@authenticate
def index():
    accaunt_list = Accaunt.query.order_by(Accaunt.family).all()
    return render_template('index.html', accaunt_list=accaunt_list)


@module.route('/new', methods=['POST', 'GET'])
@authenticate
def new_doctor():
    form = NewAccauntForm()
    if form.validate_on_submit():
        try:
            write_accaunt_to_db(form.accaunt_name.data, form.aetitle.data)
        except CertGenError:
            abort(500)        
        return redirect(url_for('accaunts.index'))
    return render_template('new_accaunt.html', form = form)


@module.route('/auth/<doctor>')
@authenticate
def send_auth_file(doctor):
    auth_data_str = get_auth_data(doctor)
    if not auth_data_str:
        abort(404)
    auth_data = BytesIO()
    auth_data.write(auth_data_str.encode())
    auth_data.seek(0)
    return send_file(auth_data, attachment_filename = 'usr.cfg', as_attachment=True, mimetype='text/csv')


@module.route('/config/<doctor>', methods=['POST', 'GET'])
@authenticate
def config_files(doctor):
    accaunt = Accaunt.query.filter_by(name=doctor).first()
    form = GetConfForm()
    form.center_name.choices = [(c.center_id, c.name) for c in Center.query.order_by('name')]
    if form.validate_on_submit():
        center = Center.query.get(form.center_name.data)
        if not center or not accaunt:
            abort(404)
        config = create_config(center.name, center.global_ip, center.lan, accaunt.family, accaunt.crt, accaunt.key)
        config_stream = BytesIO()
        config_stream.write(config.encode())
        config_stream.seek(0)
        if form.create_accaunt_on_host.data:
            create_account_on_host(center.local_ip, accaunt.name, '172.16.134.'+str(accaunt.user_id), accaunt.password), 
        return send_file(config_stream, attachment_filename = center.name+'.ovpn', as_attachment=True, mimetype='text/csv')
    return render_template('get_config.html', form = form, doctor = accaunt.family)


@module.route('/login', methods=['POST','GET'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if auth_engineer(form.username.data, form.password.data):
            session['user'] = get_eng_by_name(form.username.data).engineer_id
            return redirect(url_for('accaunts.index'))
        else:
            flash('Не верное имя пользователя или пароль')   
    return render_template('login.html', form = form)             


@module.route('/logout')
def logout():
    if session.get('user'):
        session.pop('user')
        return redirect(url_for('accaunts.login'))