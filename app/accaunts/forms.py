from xml.dom import ValidationErr
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, BooleanField, SubmitField, validators, ValidationError
class GetConfForm(FlaskForm):
    center_name = SelectField('Укажите ДЦ', validators = [], coerce=int)
    create_accaunt_on_host = BooleanField('Зарегистрировать учетную запись врача на оборудовании ДЦ')
    submit = SubmitField('Получить файл конфигурации')


class NewAccauntForm(FlaskForm):
    accaunt_name = StringField('Фамилия врача', [validators.input_required(), validators.Length(min=2, max = 60)] )
    aetitle = StringField('AETitle', [validators.Length( max = 50)])
    submit = SubmitField('Создать учетную запись  доктора')

    def validate_accaunt_name(self,accaunt_name):
        rus_alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
        for symb in accaunt_name.data.lower():
            if symb not in rus_alphabet:
                raise ValidationError('Используйте только символы кирилицы.')


class LoginForm(FlaskForm):
    username = StringField('Пользователь', [validators.DataRequired(), validators.Length(max=60)])
    password = PasswordField('Пароль', [validators.DataRequired(), validators.Length(max=15)])
    submit = SubmitField('Войти')   