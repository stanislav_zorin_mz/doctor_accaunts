from app.database import db
from .utils import create_accaunt_parametrs
from datetime import datetime
from bcrypt import hashpw, gensalt



class Accaunt(db.Model):
    __tablename__ = 'accaunts'
    user_id = db.Column(db.Integer, primary_key = True)
    family = db.Column(db.String(64), unique=True)
    name = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(64))
    aetitle = db.Column(db.String(64), unique=True)
    crt = db.Column(db.String(), unique = True)
    key = db.Column(db.String(), unique=True)
    create_time = db.Column(db.DateTime(), default = datetime.utcnow)
    
    def __repr__(self):
        return '<Accaunt %r>' % self.name





def write_accaunt_to_db(family_rus, aetitle=''):
    name, password, aetitle, crt, key = create_accaunt_parametrs(family_rus, aetitle)
    if not Accaunt.query.filter_by(name = name).first():
        user_id = get_id()
        accaunt = Accaunt(user_id = user_id, family=family_rus, name=name, password=password, aetitle=aetitle, crt=crt, key=key)
        db.session.add(accaunt)
        db.session.commit()


def get_auth_data(accaunt):
    accaunt_object = Accaunt.query.filter_by(name = accaunt).first()
    if accaunt_object:
        return accaunt_object.name+'\n'+accaunt_object.password


def get_id():
    c=[]
    query_result = db.session.query(Accaunt.user_id).all()
    busy = [i[0] for i in query_result]
    for ip in range(15,255):
        if ip not in busy:
            return ip
    return -1        
